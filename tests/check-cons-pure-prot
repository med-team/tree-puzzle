TREE-PUZZLE 

Input file name: cons-pure-prot.prot
User tree file name: cons-pure-prot.ctrees
Type of analysis: consensus construction
Parameter estimation: approximate (faster)
Parameter estimation uses: 1st user tree (for substitution process and rate variation)

Standard errors (S.E.) are obtained by the curvature method.
The upper and lower bounds of an approximate 95% confidence interval
for parameter or branch length x are x-1.96*S.E. and x+1.96*S.E.


SEQUENCE ALIGNMENT

Input data: 7 sequences with 128 amino acid sites
Number of constant sites: 7 (= 5.5% of all sites)
Number of site patterns: 125
Number of constant site patterns: 5 (= 4.0% of all site patterns)


SUBSTITUTION PROCESS

Model of substitution: Dayhoff (Dayhoff et al. 1978)
Amino acid frequencies (estimated from data set):

 pi(A) =  12.2%
 pi(R) =   2.3%
 pi(N) =   3.0%
 pi(D) =   4.9%
 pi(C) =   0.7%
 pi(Q) =   2.3%
 pi(E) =   5.4%
 pi(G) =   6.0%
 pi(H) =   5.8%
 pi(I) =   2.7%
 pi(L) =  12.5%
 pi(K) =   9.3%
 pi(M) =   1.3%
 pi(F) =   4.9%
 pi(P) =   2.6%
 pi(S) =   6.7%
 pi(T) =   4.5%
 pi(W) =   1.3%
 pi(Y) =   2.2%
 pi(V) =   9.4%


AMBIGUOUS CHARACTERS IN THE SEQUENCE (SEQUENCES IN INPUT ORDER)

               gaps  wildcards        sum   % sequence
 HBB_HUMAN        0          0          0        0.00%   
 HBB_HORSE        0          0          0        0.00%   
 HBA_HUMAN        0          0          0        0.00%   
 HBA_HORSE        0          0          0        0.00%   
 MYG_PHYCA        0          0          0        0.00%   
 GLB5_PETMA       0          0          0        0.00%   
 LGB2_LUPLU       0          0          0        0.00%   
 -------------------------------------------------------
 Sum              0          0          0        0.00%   


The table above shows the amount of gaps ('-') and other 'wildcard'
characters ('X', '?', etc.) and their percentage of the 128 columns
in the alignment.
Sequences with more than 50% ambiguous characters are marked with a '!' and 
should be checked, whether they have sufficient overlap to other sequences.
Sequences with 100% ambiguous characters do not hold any phylogenetic
information and had to be discarded from the analysis.



SEQUENCE COMPOSITION (SEQUENCES IN INPUT ORDER)

              5% chi-square test  p-value
 HBB_HUMAN         passed          92.31%  
 HBB_HORSE         passed          79.58%  
 HBA_HUMAN         passed          93.09%  
 HBA_HORSE         passed          81.20%  
 MYG_PHYCA         passed          33.23%  
 GLB5_PETMA        passed          19.11%  
 LGB2_LUPLU        passed          41.67%  

The chi-square tests compares the amino acid composition of each sequence
to the frequency distribution assumed in the maximum likelihood model.

WARNING: Result of chi-square test may not be valid because of small
maximum likelihood frequencies and short sequence length!


IDENTICAL SEQUENCES

The sequences in each of the following groups are all identical. To speed
up computation please remove all but one of each group from the data set.

 All sequences are unique.


MAXIMUM LIKELIHOOD DISTANCES

Maximum likelihood distances are computed using the selected model of
substitution and rate heterogeneity.

  7
HBB_HUMAN   0.00000  0.17425  0.96068  0.99267  2.12932  1.77231  2.78644
HBB_HORSE   0.17425  0.00000  1.01062  1.02118  2.09456  1.78465  2.86187
HBA_HUMAN   0.96068  1.01062  0.00000  0.12147  1.93540  1.30123  2.54730
HBA_HORSE   0.99267  1.02118  0.12147  0.00000  2.00515  1.39193  2.51172
MYG_PHYCA   2.12932  2.09456  1.93540  2.00515  0.00000  2.12347  2.79386
GLB5_PETMA  1.77231  1.78465  1.30123  1.39193  2.12347  0.00000  2.18272
LGB2_LUPLU  2.78644  2.86187  2.54730  2.51172  2.79386  2.18272  0.00000

Average distance (over all possible pairs of sequences):  1.73823
                  minimum  : 0.12147,  maximum  : 2.86187
                  variance : 0.65214,  std.dev. : 0.80755


RATE HETEROGENEITY

Model of rate heterogeneity: uniform rate


TREE SEARCH

100 tree topologies were specified by the user.


CONSENSUS TREE

Support for the internal branches of the unrooted consensus tree
topology is shown in percent.

This consensus tree is not completely resolved!


         :---HBA_HUMAN 
     :100:             
     :   :---HBA_HORSE 
 :100:                 
 :   :   :---MYG_PHYCA 
 :   :   :             
 :   :100:---GLB5_PETMA
 :       :             
 :       :---LGB2_LUPLU
 :                     
 :-----------HBB_HORSE 
 :                     
 :-----------HBB_HUMAN 


Consensus tree (in CLUSTAL W notation):

(HBB_HUMAN,((HBA_HUMAN,HBA_HORSE)100,(MYG_PHYCA,GLB5_PETMA,LGB2_LUPLU)100)100,
HBB_HORSE);


BIPARTITIONS

The following bipartitions occured at least once in the specified set
 of 100 usertrees tree topologies.
Bipartitions included in the consensus tree:
(bipartition with sequences in input order : number of times seen)

 **..***  :  100
 ****...  :  100
 **.....  :  100

Congruent bipartitions occurred in 50% or less, not included in 
the consensus tree:
(bipartition with sequences in input order : number of times seen)

 ****..*  :  40

Incongruent bipartitions not included in the consensus tree:
(bipartition with sequences in input order : number of times seen)

 *****..  :  37
 ****.*.  :  23


MAXIMUM LIKELIHOOD BRANCH LENGTHS ON CONSENSUS TREE (NO CLOCK)

Branch lengths are computed using the selected model of
substitution and rate heterogeneity.


              :-3 HBA_HUMAN
         :----8
         :    :-4 HBA_HORSE
 :------10
 :       :     :-------------5 MYG_PHYCA
 :       :-----9
 :             :-------6 GLB5_PETMA
 :             :
 :             :-----------------7 LGB2_LUPLU
 :
 :--2 HBB_HORSE
 :
 :-1 HBB_HUMAN
 

         branch  length     S.E.   branch  length     S.E.
HBB_HUMAN     1  0.04834  0.03148       8  0.28040  0.08746
HBB_HORSE     2  0.12685  0.03876       9  0.40362  0.12823
HBA_HUMAN     3  0.02653  0.02208      10  0.60481  0.11281
HBA_HORSE     4  0.09440  0.03145
MYG_PHYCA     5  1.33099  0.21112
GLB5_PETMA    6  0.70102  0.14139     10 iterations until convergence
LGB2_LUPLU    7  1.76831  0.28198     log L: -1699.50


Consensus tree with maximum likelihood branch lengths
(in CLUSTAL W notation):

(HBB_HUMAN:0.04834,((HBA_HUMAN:0.02653,HBA_HORSE:0.09440)100:0.28040,
(MYG_PHYCA:1.33099,GLB5_PETMA:0.70102,LGB2_LUPLU:1.76831)100:0.40362)
100:0.60481,HBB_HORSE:0.12685);


TIME STAMP

